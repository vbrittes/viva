//
//  Networker.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

enum HTTPVerb {
    case get
    case post
    case put
    case delete
    
    fileprivate var verbName: String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .delete:
            return "DELETE"
        }
    }
    
    fileprivate var containsBody: Bool {
        return self == .post || self == .put
    }
}

struct HTTPResponse {
    let result: Any?
    let error: Error?
    let rawData: Data?
}

public typealias JSONDictionary = [String: AnyHashable]

protocol Networker {
    init()
    func request(verb: HTTPVerb, action: String, parameters: JSONDictionary, headers: [String : String], completion: @escaping (_ response: HTTPResponse) -> Void)
}
