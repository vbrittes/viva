//
//  GameBasicCollectionViewCell.swift
//  Viva
//
//  Created by vmilen on 05/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit
import Kingfisher

class GameBasicCollectionViewCell: UICollectionViewCell {
    
    static let minimumWidth = CGFloat(150.0)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func fill(game: GameFill) {
        backgroundColor = .gray
        titleLabel.text = game.title
        if let url = URL(string: game.imagePath) {
            imageView.kf.setImage(with: url)
        }
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
        imageView.image = nil
    }
}
