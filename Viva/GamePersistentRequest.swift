//
//  GamePersistentRequest.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GamePersistentRequestInject: GameRequestInject {
    var gameStorageType: GameStorage.Type = GamePersistency.self
}

class GamePersistentRequest: GameRequest {
    
    private let persistencyService: GameStorage
    
    init(inject: GamePersistentRequestInject) {
        persistencyService = inject.gameStorageType.init()
        super.init(inject: inject)
    }
    
    required convenience init() {
        self.init(inject: GamePersistentRequestInject())
    }
    
    override func fetchTopGames(limit: Int, completion: @escaping ([Game]?) -> Void) {
        super.fetchTopGames(limit: limit) { games in
            guard let g = games else {
                self.persistencyService.fetchTopGames(limit: limit, completion: completion)
                return
            }
            
            self.persistencyService.saveGames(games: g)
            completion(g)
        }
    }
}
