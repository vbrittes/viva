//
//  GameDetailTableViewController.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GameDetailTableViewController: UITableViewController, GameDetailDelegate {

    private lazy var viewModel: GameDetailViewModel = GameDetailViewModel(delegate: self)
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var channelsLabel: UILabel!
    @IBOutlet weak var viewersLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let fill = viewModel.gameFill()
        
        if let url = URL(string: fill.imagePath) {
            imageView.kf.setImage(with: url)
        }
        
        navigationItem.title = fill.title
        channelsLabel.text = fill.formattedChannels
        viewersLabel.text = fill.formattedViewers
    }
    
    func prepareForNavigation(object: AnyObject) {
        viewModel.prepareForNavigation(object: object)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        tableView.reloadData()
    }
    
    //MARK: GameDetailDelegate
    func updatedGameDetail() {
        navigationItem.title = viewModel.gameFill().title
    }
    
    //MARK: UITableViewDataSource & UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
        }
        return indexPath.row == 0 ? view.frame.height / 2 : UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
}
