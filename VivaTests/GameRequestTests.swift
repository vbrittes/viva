//
//  GameRequestTests.swift
//  Viva
//
//  Created by vmilen on 06/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import XCTest
@testable import Viva

class GameRequestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFetch() {
        let e = expectation(description: "\(#file):\(#line)")
        
        let request = GameRequest()
        request.fetchTopGames(limit: 50) { games in
            XCTAssertEqual(games?.count, 50)
            e.fulfill()
        }
        
        waitForExpectations(timeout: 300) { error in
            XCTAssertNil(error)
        }
    }

    
}
