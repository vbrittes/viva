//
//  GamesListCollectionViewController.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit
import Kingfisher

class GamesListCollectionViewController: UICollectionViewController, UICollectionViewDataSourcePrefetching, GamesListDelegate {
    
    private lazy var viewModel: GamesListViewModel = GamesListViewModel(delegate: self)
    
    private let refreshControl = UIRefreshControl()
    
    private var cellsPerSection: Int {
        return Int(view.frame.width/GameBasicCollectionViewCell.minimumWidth)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupRefreshControl()
        setupFlowLayout()
        viewModel.loadGames()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        self.collectionView?.contentOffset = .zero
        
        if let layout = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.invalidateLayout()
        }
        
        coordinator.animateAlongsideTransition(in: nil, animation: nil) { ctx in
            self.setupFlowLayout()
            self.collectionView?.reloadData()
        }
    }
    
    private func setupFlowLayout() {
        guard let collectionView = collectionView else {
            return
        }
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.invalidateLayout()
        }
        
        let side = view.frame.width/CGFloat(cellsPerSection)
        
        let newLayout = UICollectionViewFlowLayout()
        
        newLayout.itemSize = viewModel.errorMessage == nil ? CGSize(width: side, height: side) : collectionView.frame.size
        newLayout.minimumLineSpacing = 0
        newLayout.minimumInteritemSpacing = 0
        collectionView.collectionViewLayout = newLayout
    }

    private func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(GamesListCollectionViewController.pullToRefreshAction), for: .valueChanged)
        collectionView?.alwaysBounceVertical = true
        collectionView?.addSubview(refreshControl)
    }
    
    // MARK: Actions
    func pullToRefreshAction() {
        viewModel.loadGames()
    }
    
    // MARK: GamesListDelegate
    func updatedGamesList() {
        DispatchQueue.main.async {
            self.setupFlowLayout()
            self.collectionView?.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.errorMessage == nil ? Int(ceil(Double(viewModel.gameCount)/Double(cellsPerSection))) : 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.errorMessage == nil ? cellsPerSection : 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.errorMessage != nil {
            return noResultCell(collectionView: collectionView, indexPath: indexPath)
        }
        
        return gameCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    private func noResultCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NoResultCollectionViewCell.self), for: indexPath) as! NoResultCollectionViewCell
        cell.title = viewModel.errorMessage
        cell.reaction = viewModel.reaction
        
        return cell
    }
    
    private func gameCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GameBasicCollectionViewCell.self), for: indexPath) as! GameBasicCollectionViewCell
        let index = indexPath.section * cellsPerSection + indexPath.item
        
        if let game = viewModel.gameFill(index: index) {
            cell.fill(game: game)
        }
        
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if viewModel.errorMessage == nil {
            performSegue(withIdentifier: "pushDetail", sender: indexPath)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let indexPath = sender as? IndexPath,
            let detailVC = segue.destination as? GameDetailTableViewController,
            let navigationData = viewModel.gameNavigationData(index: indexPath.section * cellsPerSection + indexPath.item) else {
            return
        }
        
        detailVC.prepareForNavigation(object: navigationData)
    }
    
    // MARK: UICollectionViewDataSourcePrefetching
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard let indexPath = indexPaths.first else {
            return
        }
        
        let index = indexPath.section * cellsPerSection + indexPath.item
        
        if let game = viewModel.gameFill(index: index), let url = URL(string: game.imagePath) {
            KingfisherManager.shared.downloader.downloadImage(with: url)
        }
    }

}
