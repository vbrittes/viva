//
//  GameRequest.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GameRequestInject {
    var networkerType: Networker.Type = AlamofireNetworker.self
}

class GameRequest: GameService {

    private let networker: Networker
    
    required convenience init() {
        self.init(inject: GameRequestInject())
    }
    
    init(inject: GameRequestInject) {
        self.networker = inject.networkerType.init()
    }
    
    func fetchTopGames(limit: Int, completion: @escaping ([Game]?) -> Void) {
        networker.request(verb: .get, action: Endpoint.games.path, parameters: ["limit" : limit], headers: Endpoint.globalHeader) { response in
            guard let responseJSON = response.result as? JSONDictionary, let gamesJSON = responseJSON["top"] as? [JSONDictionary] else {
                completion(nil)
                return
            }
            
            let games = gamesJSON.flatMap { Game(dict: $0) }
            completion(games)
        }
    }
}
