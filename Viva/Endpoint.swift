//
//  Endpoint.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

enum Endpoint: String {
    static var globalHeader: [String : String] {
        return ["Client-ID" : "de3z2svfuy1hv8l6zdap7wbu7ktni4"]
    }
    
    private var rootURL: URL {
        return URL(string: "https://api.twitch.tv")!
    }

    case games = "kraken/games/top"
    
    var path: String {
        return URL(string: rawValue, relativeTo: rootURL)!.absoluteString
    }
    
}
