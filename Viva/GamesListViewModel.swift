//
//  GamesListViewModel.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GamesListInject {
    var gameServiceType: GameService.Type = GamePersistentRequest.self
}

struct GameFill {
    var title: String
    var imagePath: String
}

enum ReactionType: String {
    case looking = "🕵🏽"
    case sad = "☹️"
}

protocol GamesListDelegate: class {
    func updatedGamesList()
}

class GamesListViewModel: NSObject {
    
    private let gameService: GameService
    private let fetchLimit = 50
    
    private var games = [Game]() {
        didSet {
            if games.isEmpty {
                errorMessage = "Nenhum resultado encontrado"
                reaction = .sad
            }
        }
    }
    
    var errorMessage: String? = "Carregando..." {
        didSet {
            if errorMessage == nil {
                reaction = nil
            }
        }
    }
    var reaction: ReactionType? = .looking
    
    var gameCount: Int {
        return games.count
    }
    
    weak var delegate: GamesListDelegate?
    
    init(delegate: GamesListDelegate?, inject: GamesListInject = GamesListInject()) {
        self.delegate = delegate
        gameService = inject.gameServiceType.init()
    }
    
    func loadGames() {
        gameService.fetchTopGames(limit: fetchLimit) { games in
            guard let games = games else {
                self.games = []
                self.delegate?.updatedGamesList()
                return
            }
            
            self.errorMessage = nil
            self.games = games
            self.delegate?.updatedGamesList()
        }
    }
    
    func gameFill(index: Int) -> GameFill? {
        guard let game = games.object(index: index) else {
            return nil
        }
        
        let fill = GameFill(title: game.name, imagePath: game.logo?.medium ?? "")
        
        return fill
    }
    
    func gameNavigationData(index: Int) -> AnyObject? {
        return games.object(index: index) as AnyObject?
    }
}
