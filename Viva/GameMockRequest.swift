//
//  GameMockRequest.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GameMockRequest: GameService {
    var success = true
    
    required init() {
    }
    
    func fetchTopGames(limit: Int, completion: @escaping ([Game]?) -> Void) {
        let path = Bundle.main.path(forResource: "Game", ofType: "json")
        
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! [String : AnyHashable?]
        let gamesResult = jsonResult["top"] as! [[String : AnyHashable?]]
        
        let games = gamesResult.flatMap { Game(dict: $0) }
        
        completion(success ? games : nil)
    }
}

class FailGameMockRequest: GameMockRequest {
    required init() {
        super.init()
        super.success = false
    }
}
