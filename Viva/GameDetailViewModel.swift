//
//  GameDetailViewModel.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class GamesDetailInject {
    //for now, empty
}

struct GameDetailFill {
    var title: String
    var imagePath: String
    var formattedViewers: String
    var formattedChannels: String
}

struct GameNavigationData {
    var game: Game
}

protocol GameDetailDelegate: class {
    func updatedGameDetail()
}

class GameDetailViewModel {
    
    private var game: Game?
    
    weak var delegate: GameDetailDelegate?
    
    init(delegate: GameDetailDelegate?, inject: GamesDetailInject = GamesDetailInject()) {
        self.delegate = delegate
    }
    
    func gameFill() -> GameDetailFill {
        guard let game = game else {
            return GameDetailFill(title: "", imagePath: "", formattedViewers: "", formattedChannels: "")
        }
        return GameDetailFill(title: game.name, imagePath: game.logo?.large ?? "", formattedViewers: "\(game.viewers)", formattedChannels: "\(game.channels)")
    }
    
    func prepareForNavigation(object: AnyObject) {
        if let game = object as? Game {
            self.game = game
            delegate?.updatedGameDetail()
        }
    }
    
}
