//
//  GameListViewModelTests.swift
//  Viva
//
//  Created by vmilen on 10/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import XCTest
@testable import Viva

class GameListClosures: GamesListDelegate {
    var updatedGamesListClosure: (() -> Void)?
    
    func updatedGamesList() {
        updatedGamesListClosure?()
    }
}

class GameListViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSuccessFetch() {
        let inject = GamesListInject()
        inject.gameServiceType = GameMockRequest.self
        
        let e = expectation(description: "\(#file):\(#line)")
        
        let delegate = GameListClosures()
        delegate.updatedGamesListClosure = { _ in
            e.fulfill()
        }
        
        let viewModel = GamesListViewModel(delegate: delegate, inject: inject)
        viewModel.loadGames()
        
        waitForExpectations(timeout: 15) { err in
            XCTAssertNil(err)
        }
        
        XCTAssertEqual(viewModel.gameCount, 50)
        XCTAssertNil(viewModel.errorMessage)
        XCTAssertNil(viewModel.reaction)
        XCTAssertNil(viewModel.gameFill(index: 51))
        
        let fill = viewModel.gameFill(index: 0)
        XCTAssertNotNil(fill)
        XCTAssertEqual(fill?.title, "League of Legends")
        XCTAssertEqual(fill?.imagePath, "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg")
    }
    
    func testFailureFetch() {
        let inject = GamesListInject()
        inject.gameServiceType = FailGameMockRequest.self
        
        let e = expectation(description: "\(#file):\(#line)")
        
        let delegate = GameListClosures()
        delegate.updatedGamesListClosure = { _ in
            e.fulfill()
        }
        
        let viewModel = GamesListViewModel(delegate: delegate, inject: inject)
        viewModel.loadGames()
        
        waitForExpectations(timeout: 15) { err in
            XCTAssertNil(err)
        }
        
        XCTAssertEqual(viewModel.gameCount, 0)
        XCTAssertNil(viewModel.gameFill(index: 0))
        XCTAssertEqual(viewModel.errorMessage, "Nenhum resultado encontrado")
        XCTAssertEqual(viewModel.reaction, .sad)
        
    }
    
}
