//
//  AlamofireNetworker.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit
import Alamofire

private extension HTTPVerb {
    var alamofireMatch: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        case .delete:
            return .delete
        }
    }
}

class AlamofireNetworker: Networker {
    required init() {
    }
    
    internal func request(verb: HTTPVerb, action: String, parameters: JSONDictionary, headers: [String : String], completion: @escaping (HTTPResponse) -> Void) {
        Alamofire.request(action, method: verb.alamofireMatch, parameters: parameters, headers: headers).responseJSON { response in
            completion(HTTPResponse(result: response.result.value, error: response.error, rawData: response.data))
        }
    }
}
