//
//  GameTests.swift
//  Viva
//
//  Created by vmilen on 04/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import XCTest
@testable import Viva

class GameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDeserialization() {
        let mock = GameMockRequest()
        
        let e = expectation(description: "\(#file):\(#line)")
        
        mock.fetchTopGames(limit: 50) { games in
            guard let games = games, !games.isEmpty else {
                XCTFail("Deserialization error")
                e.fulfill()
                return
            }
            
            guard let game = games.first else {
                XCTFail()
                e.fulfill()
                return
            }
            
            XCTAssertEqual(games.count, 50)
            
            XCTAssertEqual(game.viewers, 125054)
            XCTAssertEqual(game.popularity, 128521)
            XCTAssertEqual(game.name, "League of Legends")
            XCTAssertEqual(game.logo?.small, "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg")
            XCTAssertEqual(game.logo?.medium, "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg")
            XCTAssertEqual(game.logo?.large, "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg")
            XCTAssertEqual(game.logo?.template, "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-{width}x{height}.jpg")
            XCTAssertEqual(game.id, 21779)
            XCTAssertEqual(game.giantbombId, 24024)
            XCTAssertEqual(game.channels, 1787)
            XCTAssertEqual(game.box?.small, "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg")
            XCTAssertEqual(game.box?.medium, "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg")
            XCTAssertEqual(game.box?.large, "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg")
            XCTAssertEqual(game.box?.template, "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-{width}x{height}.jpg")
            
            e.fulfill()
        }
        
        waitForExpectations(timeout: 300) { error in
            XCTAssertNil(error)
        }
    }
    
}
