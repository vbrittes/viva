//
//  GameDetailViewModelTests.swift
//  Viva
//
//  Created by vmilen on 10/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import XCTest
@testable import Viva

class GameDetailClosures: GameDetailDelegate {
    var updatedGameDetailClosure: (() -> Void)?
    
    func updatedGameDetail() {
        updatedGameDetailClosure?()
    }
}

class GameDetailViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetch() {
        let inject = GamesDetailInject()
        
        var calledDelegate = false
        
        let delegate = GameDetailClosures()
        delegate.updatedGameDetailClosure = { _ in
            calledDelegate = true
        }
        
        var game = Game()
        game.name = "theName"
        game.channels = 11
        game.viewers = 22
        var img = ImageDimensions()
        img.large = "large"
        game.logo = img
        
        let viewModel = GameDetailViewModel(delegate: delegate, inject: inject)
        viewModel.prepareForNavigation(object: game as AnyObject)
        
        XCTAssert(calledDelegate)
        
        let fill = viewModel.gameFill()
        XCTAssertEqual(fill.title, game.name)
        XCTAssertEqual(fill.imagePath, img.large)
        XCTAssertEqual(fill.formattedViewers, "\(game.viewers)")
        XCTAssertEqual(fill.formattedChannels, "\(game.channels)")
    }
    
}
