//
//  NoResultCollectionViewCell.swift
//  Viva
//
//  Created by vmilen on 09/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

class NoResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reactionLabel: UILabel!
    
    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    var reaction: ReactionType? {
        didSet {
            reactionLabel.text = reaction?.rawValue
        }
    }
}
