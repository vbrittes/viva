//
//  ImageDimensions.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

protocol ImageDimensionsRepresentation {
    init()
    var large: String { get set }
    var medium: String { get set }
    var small: String { get set }
    var template: String { get set }
}

extension ImageDimensionsRepresentation {
    func convertToRepresentation<T>(type: T.Type) -> T where T: ImageDimensionsRepresentation {
        var representation = type.init()
        representation.large = self.large
        representation.medium = self.medium
        representation.small = self.small
        representation.template = self.template
        
        return representation
    }
}

struct ImageDimensions: ImageDimensionsRepresentation {
    var large = ""
    var medium = ""
    var small = ""
    var template = ""
    
    init() { }
    
    init?(dict: [String : AnyHashable?]?) {
        guard let dict = dict else {
            return nil
        }
        
        large = dict["large"] as? String ?? ""
        medium = dict["medium"] as? String ?? ""
        small = dict["small"] as? String ?? ""
        template = dict["template"] as? String ?? ""
    }
}
