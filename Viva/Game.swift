//
//  Game.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

protocol GameRepresentation {
    associatedtype ImageType: ImageDimensionsRepresentation
    init()
    var name: String { get set }
    var popularity: Int { get set }
    var id: Int { get set }
    var giantbombId: Int { get set }
    var box: ImageType? { get set }
    var logo: ImageType? { get set }
    var viewers: Int { get set }
    var channels: Int { get set }
}

extension GameRepresentation {
    func convertToRepresentation<T>(type: T.Type) -> T where T: GameRepresentation {
        var representation = type.init()
        representation.name = self.name
        representation.popularity = self.popularity
        representation.id = self.id
        representation.giantbombId = self.giantbombId
        representation.viewers = self.viewers
        representation.channels = self.channels
        
        representation.box = self.box?.convertToRepresentation(type: T.ImageType.self)
        representation.logo = self.logo?.convertToRepresentation(type: T.ImageType.self)
        
        return representation
    }
}

struct Game: GameRepresentation {
    
    internal init() { }
    
    var name = ""
    var popularity = 0
    var id = 0
    var giantbombId = 0
    var box: ImageDimensions?
    var logo: ImageDimensions?
    var viewers = 0
    var channels = 0
    
    init?(dict: [String : AnyHashable?]?) {
        guard let dict = dict, let gameDict = dict["game"] as? [String : AnyHashable?] else {
            return nil
        }
        
        guard
            let id = gameDict["_id"] as? Int,
            let giantbombId = gameDict["giantbomb_id"] as? Int else {
            return nil
        }
        
        self.id = id
        self.giantbombId = giantbombId
        
        name = gameDict["name"] as? String ?? ""
        popularity = gameDict["popularity"] as? Int ?? 0
        box = ImageDimensions(dict: gameDict["box"] as? [String : AnyHashable?])
        logo = ImageDimensions(dict: gameDict["logo"] as? [String : AnyHashable?])
        viewers = dict["viewers"] as? Int ?? 0
        channels = dict["channels"] as? Int ?? 0
    }
}
