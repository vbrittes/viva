//
//  GameService.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

protocol GameService {
    init()
    func fetchTopGames(limit: Int, completion: @escaping ([Game]?) -> Void)
}

protocol GameStorage: GameService {
    func saveGames(games: [Game])
}
