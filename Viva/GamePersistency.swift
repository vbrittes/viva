//
//  GamePersistency.swift
//  Viva
//
//  Created by vmilen on 03/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class RealmGame: Object, GameRepresentation {
    typealias ImageType = RealmImageDimensions
    
    dynamic var name = ""
    dynamic var popularity = 0
    dynamic var id = 0
    dynamic var giantbombId = 0
    dynamic var box: ImageType?
    dynamic var logo: ImageType?
    dynamic var viewers = 0
    dynamic var channels = 0
    
}

class RealmImageDimensions: Object, ImageDimensionsRepresentation {
    dynamic var large = ""
    dynamic var medium = ""
    dynamic var small = ""
    dynamic var template = ""
}

class GamePersistency: GameStorage {
    required init() {
    }
    
    func fetchTopGames(limit: Int, completion: @escaping ([Game]?) -> Void) {
        let games = try! Realm().objects(RealmGame.self).enumerated().map { $0.element.convertToRepresentation(type: Game.self) }
        completion(games)
    }
    
    func saveGames(games: [Game]) {
        let realm = try! Realm()
        
        let realmGames = games.map { $0.convertToRepresentation(type: RealmGame.self) }
        
        try! realm.write {
            realm.deleteAll()
            realm.add(realmGames)
        }
    }
}
