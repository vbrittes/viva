//
//  ArrayExtension.swift
//  Viva
//
//  Created by vmilen on 06/05/17.
//  Copyright © 2017 vbrites. All rights reserved.
//

import UIKit

extension Array {
    func object(index: Int) -> Element? {
        if index >= 0 && index < self.count {
            return self[index]
        }
        return nil
    }
}
